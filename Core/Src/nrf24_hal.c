#include "nrf24_hal.h"


// Configure the GPIO lines of the nRF24L01 transceiver
// note: IRQ pin must be configured separately
void nRF24_GPIO_Init(void) {
    //GPIO_InitTypeDef PORT;

    // Enable the nRF24L01 GPIO peripherals
	RCC->APB2ENR |= nRF24_GPIO_PERIPHERALS;

    // Configure CSN pin
	//PORT.GPIO_Mode = GPIO_Mode_Out_PP;
	//PORT.GPIO_Speed = GPIO_Speed_2MHz;
	//PORT.GPIO_Pin = nRF24_CSN_PIN;
	//GPIO_Init(nRF24_CSN_PORT, &PORT);
	nRF24_CSN_H;

	// Configure CE pin
	//PORT.GPIO_Pin = nRF24_CE_PIN;
	//GPIO_Init(nRF24_CE_PORT, &PORT);
	nRF24_CE_L;
}

// Low level SPI transmit/receive function (hardware depended)
// input:
//   data - value to transmit via SPI
// return: value received from SPI
uint8_t nRF24_LL_RW(uint8_t txData) {
	 // Wait until TX buffer is empty

	//while (SPI_I2S_GetFlagStatus(nRF24_SPI_PORT, SPI_FLAG_TXE) == RESET);
	// Send byte to SPI (TXE cleared)

	//SPI_I2S_SendData(nRF24_SPI_PORT, data);
	HAL_SPI_Transmit(nRF24_SPI_PORT, &txData, sizeof(uint8_t), 100);
	// Wait while receive buffer is empty
	//while (__HAL_SPI_GET_FLAG(nRF24_SPI_PORT, SPI_FLAG_RXNE) == RESET);

	// Return received byte
	uint8_t rxData=0;
	HAL_SPI_Receive(nRF24_SPI_PORT, &rxData, sizeof(uint8_t), 100);
	return rxData;
}
